![heapnosis](docs/images/logo.png)

# heapnosis

A GDB plugin for heap memory tracking and corruption detections.

![heapnosis_example](docs/images/uaf.png)

## Overview

heapnosis is a GDB plugin for reverse-engineering and vulnerability researches. It aims to track heap memory allocation. It also has a feature for heap memory corruption detection. It currently only supports x86_64 architecture.

## Quick setup

Very simple to install:

```
curl -o ~/.gdb_heapnosis.py https://gitlab.com/cyrillec/heapnosis/-/raw/master/heapnosis.py
echo 'source ~/.gdb_heapnosis.py' >> ~/.gdbinit
```

Or, simply import the python script into your gdb session.

```
(gdb) source heapnosis.py
```

## Features

Heapnosis basic features
 - heapnosis tracks memory allocations
 - heapnosis can list tracked chunks and to **display history of chunks**
 - heapnosis **detects heap memory corruptions** 
 - heapnosis does not require any recompilation nor instrumentation.
 - heapnosis can be enable/disable at any moment on a **running processes**.

Note: In order to keep history of allocations chunks are never free (free calls are bypassed).

Supported memory corruptions:
 - Out Of Bound (on write access)
 - Use After Free (on write access)
 - Double Free 

## Limitations

This plugin is not perfect:
 - It only supports x86_64 arch (for the moment)
 - Memory corruption detections use GDB Software Watchpoints (as Hardware watchpoints are limited to 4 entries).
   * It is very very slow (the feature can be disabled)
   * It only supports write memory access for detection

## Quick start

I provide an example binary in `tests/` directory.

```
$ cd tests
$ make
$ cd ..
```

Launch the binary with gdb. And stop it when you want to start your investigations.
**I do not recommend to use heapnosis before binary entry point execution**

```
$ gdb tests/heap_san_tester
...
(gdb) br *main
(gdb) run
```

Print heapnosis help. (There are also `help` menus for `chunks` and `guards` submenus).

```
(gdb) heapnosis help
heapnosis help -- display this
heapnosis enable -- install malloc/free catchers in order to track heap allocations
heapnosis disable -- remove malloc/free catchers
heapnosis status -- return heapnosis malloc/free catchers status
heapnosis chunks -- list/display/clean tracked allocations
heapnosis guards -- manage autoinstalled watchpoints that track memory corruptions
(gdb)
```

Enable heapnosis and continue

```
(gdb) heapnosis enable
Breakpoint 2 at 0x4006b0 (2 locations)
Breakpoint 3 at 0x4006a0 (3 locations)
Breakpoint 4 at 0x4006c0 (3 locations)
Breakpoint 5 at 0x400640
(gdb) continue
```

Trigger an out of bound.

![heapnosis_quickstart](docs/images/quick_start.png)

## Advanced usage

You can also disable watchpoints and guards.

```
(gdb) heapnosis guards disable
```

This will allow you to track memory allocations and their history without the overload of watchpoints.

```
(gdb) heapnosis chunks list
0x602018 (0x8 bytes): freed
(gdb) heapnosis chunks 0x602018
```

Example:

![heapnosis_concepts](docs/images/advanced.png)

## How does it works?

### Tracking

heapnosis install breakpoints on:
 - malloc
 - calloc
 - realloc
 - free

When triggered, it saves context allocation (size, thread, backtrace, etc.) and it launch a temporary breakpoint to get execution result (chunk address).

Notes:
 - free breakpoint bypass real free execution
 - calloc and realloc are not really executed but instead redirect to a malloc

### Memory corruption detection

Memory corruption detection works like address sanitizers on compilators. It adds (before and after) the chunk memory spaces that should not be accessed by the program. These memory spaces are called `guards`.

The plugin uses gdb watchpoints to detect memory access on `guards`. There are only 4 hardware watchpoints available per process. This is not enough for this use case. This is the reason why heapnosis uses software watchpoints. Unfortunatly software watchpoints are really slow and it only supports write memory access.

Watchpoints are added on freed memories in order to detect use after free.

![heapnosis_concepts](docs/images/concept.png)

## Enjoy

Best regards,

Cyrille Chatras

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

size_t s = 8;
unsigned char *ptr;

unsigned int user_choice(){
  unsigned int res;
  unsigned int choice;
  res = scanf("%d",&choice);
  if (res == 0){
    printf("Invalid input\n");
    exit(0);
  }
  return choice;
}

void print_ptr(){
  printf("[ ");
  for(int i = 0; i < (s-1) ; i++){
    printf("0x%02x, ",ptr[i]);
  }
   printf("0x%02x ]\n",ptr[s-1]);
}
void main(){
  unsigned int choice;
  unsigned int res;
  ptr = malloc(s);
  memset(ptr, 0, s); 
  print_ptr();
  while(1){
    printf("Choice:\n [1] free\n [2] malloc\n [3] calloc\n [4] realloc\n [5] write\n [6] quit\n > ");
    choice = user_choice();
    printf("Your choice %d\n",choice);
    switch (choice){
      case 1:
        free(ptr);
        break;
      case 2:
        printf("New size\n > ");
        choice = user_choice();
        s = (size_t) choice;
        ptr = malloc(s);
        break;
      case 3:
        printf("Nb elem\n > ");
        int nb_elem = user_choice();
        printf("Elem Size\n >");
        choice = user_choice();
        s = (size_t) (nb_elem*choice);
        ptr = calloc(nb_elem, choice);
        break;
      case 4:
        printf("New size\n > ");
        choice = user_choice();
        s = (size_t) choice;
        ptr = realloc(ptr, s);
        break;
      case 5:
        printf("New offset\n > ");
        choice = user_choice();
        printf("New value\n > ");
        ptr[(int) choice] = (unsigned char) user_choice();
        break;
      case 6:
        exit(0);
    }
    print_ptr();
  }  
}

try:
  import gdb
except ImportError:
  raise Exception("This script can only be run with GDB")

import struct
import logging

GUARD_SIZE = 0x8
GUARD_EXP = "* (char[%d] *)"%GUARD_SIZE

"""
<Helpers>
"""

class Colored:
  """
  Pretty print coloring
  """
  RED = '\033[91m'
  GREEN = '\033[92m'
  BLUE = '\033[94m'
  END = '\033[0m'
  def red(s):
    return Colored.RED + s + Colored.END
  def blue(s):
    return Colored.BLUE + s + Colored.END
  def green(s):
    return Colored.GREEN + s + Colored.END

allocated_chunks = {}

def current_status():
  """
  Get current status of breakpoint
  Return a tuple:
   - current thread with current pc and function
   - backtrace
  """
  thread = gdb.selected_thread()
  pc = gdb.selected_frame().pc() 
  symbol = "(in %s)"%gdb.selected_frame().name()
  bt = gdb.execute('bt', to_string=True)
  return ("thread %d: at 0x%x %s"%(thread.num, pc, symbol), bt) 

def display_allocation(chunk):
  """
  Pretty prints an allocation status
  """
  if chunk not in allocated_chunks:
    return
  if allocated_chunks[chunk]["freed"]:
    print( Colored.blue( "Freed by: "+allocated_chunks[chunk]["freed"][0] ) )
    print( allocated_chunks[chunk]["freed"][1] )
  if allocated_chunks[chunk]["allocated"]:
    print( Colored.green( "Allocated by: "+allocated_chunks[chunk]["allocated"][0] ) )
    print( allocated_chunks[chunk]["allocated"][1] )

def Report( title , chunk):
  """
  Pretty print report for OOB, UAF or Double Free
  """
  print("\n")
  print( Colored.red(title) )
  print("Chunk at 0x%x (size: 0x%x)\n"%(chunk, allocated_chunks[chunk]["size"]))
  status = current_status()
  print( Colored.red( "Write by: "+status[0] ) )
  print( status[1] )
  display_allocation(chunk)
  print("\n")

"""
<Inheritable classes>
"""

class Guard( gdb.Breakpoint):
  """
  Inheritable class for Guards.
  If guard is enable, it creates a watchpoint.
  """ 
  def __init__(self, chunk, *args, **kwargs):
    global guards
    if guards.enabled:
      guards.append(self)
      self.chunk = chunk
      super().__init__( *(args + (gdb.BP_WATCHPOINT,)), **kwargs )

class Hook( gdb.Breakpoint):
  """
  Inheritable class for Catchers.
  """
  def __init__(self, *args, **kwargs):
    super().__init__( *args, **kwargs )

"""
<Guards>
Guards raise reports when hit
"""

class InstallUAFGuard( Guard ):
  def stop(self):
    Report("Use after free write detected", self.chunk)
    return True

class InstallOOBGuard( Guard ):
  def stop(self):
    Report("Heap Out Of Bound write detected", self.chunk) 
    return True

"""
<Hooks/Catchers>
Hooks and Catchers track allocations and free
"""

class AllocGuardInstaller ( gdb.Breakpoint ):
  """
  AllocGuardInstaller track malloc result and install guard watchpoints
  """
  def __init__(self, size, rdi, *args, **kwargs):
    self.malloc_size = size
    self.rdi = rdi
    super().__init__( *args, **kwargs )

  def stop( self ):
    old_reg_rax = int( gdb.parse_and_eval("$rax") )
    if guards.enabled:
      gdb.execute("set variable $rax = 0x%x"%(old_reg_rax+GUARD_SIZE))
    gdb.execute("set variable $rdi = 0x%x"%(self.rdi))
    reg_rax = int( gdb.parse_and_eval("$rax") )
    logging.info("AllocGuardInstaller: Catch allocation at 0x%x (size: 0x%x)"%(reg_rax, self.malloc_size))
    allocated_chunks[reg_rax] = { "size":self.malloc_size, "freed":False, "allocated":current_status() }
    logging.debug("AllocGuardInstaller: InstallOOBGuard")
    InstallOOBGuard(reg_rax, GUARD_EXP+" 0x%x"%(reg_rax-GUARD_SIZE))
    InstallOOBGuard(reg_rax, GUARD_EXP+" 0x%x"%(reg_rax+self.malloc_size))

class MallocCatcher( Hook ):
  """
  MallocCatcher:
   - it get size allocation
   - add guards if enabled
   - install temporary breakpoint (named AllocGuardInstaller) to catch allocation result
  """
  def stop( self ):
    initial_rdi = initial_malloc_size = int( gdb.parse_and_eval("$rdi") )
    logging.info("MallocCatcher: malloc(0x%x)"%initial_malloc_size) 
    if initial_malloc_size == 0:
      return False
    if guards.enabled:
      new_malloc_size = initial_malloc_size + GUARD_SIZE*2
      gdb.execute("set variable $rdi = 0x%x"%new_malloc_size)
      logging.debug("MallocCatcher: -> add guard: new size = 0x%x"%new_malloc_size)
    reg_rsp = gdb.parse_and_eval("$rsp")
    return_address = gdb.selected_inferior().read_memory(reg_rsp, 8).tobytes()
    return_address = struct.unpack("<Q", return_address)[0]
    logging.debug("MallocCatcher: <- return at 0x%x (install AllocGuardInstaller)"%return_address)
    AllocGuardInstaller(initial_malloc_size, initial_rdi, "*0x%x"%return_address, temporary=True)
    return False

class CallocCatcher( Hook ):
  """
  CallocCatcher:
   - it calculates size allocation depending on nb element ad element size
   - add guards if enabled
   - install temporary breakpoint (named AllocGuardInstaller) to catch allocation result
   - it then call standard malloc instead of calloc
  """
  def stop( self ):
    elem_size = int( gdb.parse_and_eval("$rsi") )
    initial_rdi = nb_elem = int( gdb.parse_and_eval("$rdi") )
    logging.info("CallocCatcher: calloc(0x%x, 0x%x)"%(nb_elem,elem_size))
    initial_alloc_size = (nb_elem*elem_size)
    if guards.enabled:
       new_malloc_size = initial_alloc_size + GUARD_SIZE*2
    else:
       new_malloc_size = initial_alloc_size
    gdb.execute("set variable $rdi = 0x%x"%new_malloc_size)
    gdb.execute("set variable $rip = malloc")
    malloc_size = int( gdb.parse_and_eval("$rdi") )
    logging.debug("CallocCatcher: -> add guard: new size = 0x%x"%malloc_size)
    reg_rsp = gdb.parse_and_eval("$rsp")
    return_address = gdb.selected_inferior().read_memory(reg_rsp, 8).tobytes()
    return_address = struct.unpack("<Q", return_address)[0]
    logging.debug("CallocCatcher: <- return at 0x%x (install AllocGuardInstaller)"%return_address)
    AllocGuardInstaller(initial_alloc_size, initial_rdi, "*0x%x"%return_address, temporary=True)
    return False


class FreeCatcher( Hook ):
  """
  FreeCatcher:
  - it check if the pointed chunk is tracked
  - if chunk is tracked and not freed it install a guard on it
  - if chunk is tracked and alread freed it raise a report for a double free
  Note: real free is bypass
  """
  def stop( self ):
    chunk = int( gdb.parse_and_eval("$rdi") )
    logging.info("FreeCatcher: free( 0x%x )"%chunk)
    if chunk in allocated_chunks:
      if allocated_chunks[chunk]["freed"]:
        Report("Double free detected", chunk)
        return True
      else:
        exp = "* (char[%d] *)"%allocated_chunks[chunk]["size"] 
        logging.debug("FreeCatcher: InstallUAFGuard")
        InstallUAFGuard(chunk, exp+" 0x%x"%chunk)
        allocated_chunks[chunk]["freed"] = current_status()
        #Fake "ret" instruction to not call real free
        reg_rsp = gdb.parse_and_eval("$rsp")
        return_address = gdb.selected_inferior().read_memory(reg_rsp, 8).tobytes()
        return_address = struct.unpack("<Q", return_address)[0]
        logging.info("FreeCatcher: <- return at 0x%x (faking 'ret' instruction)"%return_address)
        gdb.execute("set variable $rip = 0x%x"%return_address)
        gdb.execute("set variable $rsp = $rsp+8")
    return False

class ReallocCatcher( Hook ):
  """
  ReallocCatcher
  - it check if the pointed chunk is tracked
    - if chunk is tracked and not freed it install a guard on it
    - if chunk is tracked and alread freed it raise a report for a double free
  - it then do a standard malloc (with added guard if enabled)
  Note: ptr is never really freed
  """
  def stop( self ):
    initial_alloc_size = int( gdb.parse_and_eval("$rsi") )
    initial_rdi = ptr = int( gdb.parse_and_eval("$rdi") )
    logging.info("CallocCatcher: calloc(0x%x, 0x%x)"%(ptr,initial_alloc_size))
    if ptr in allocated_chunks:
      if allocated_chunks[ptr]["freed"]:
        Report("Double free detected via realloc()", ptr)
        return True
      else:
        exp = "* (char[%d] *)"%allocated_chunks[ptr]["size"] 
        logging.info("ReallocCatcher: InstallUAFGuard")
        InstallUAFGuard(ptr, exp+" 0x%x"%ptr)
        allocated_chunks[ptr]["freed"] = current_status()
    if guards.enabled:
      new_malloc_size = initial_alloc_size + GUARD_SIZE*2
    else:
      new_malloc_size = initial_alloc_size
    gdb.execute("set variable $rdi = 0x%x"%new_malloc_size)
    gdb.execute("set variable $rip = malloc")
    malloc_size = int( gdb.parse_and_eval("$rdi") )
    logging.debug("ReallocCatcher: -> add guard: new size = 0x%x"%malloc_size)
    reg_rsp = gdb.parse_and_eval("$rsp")
    return_address = gdb.selected_inferior().read_memory(reg_rsp, 8).tobytes()
    return_address = struct.unpack("<Q", return_address)[0]
    logging.debug("ReallocCatcher: <- return at 0x%x (install AllocGuardInstaller)"%return_address)
    AllocGuardInstaller(initial_alloc_size, initial_rdi, "*0x%x"%return_address, temporary=True)
    return False

"""
Controllers
"""
class Hooks():
  def __init__(self):
    self.hooks=[]
    #self.install()
  def install(self):
    gdb.execute("set can-use-hw-watchpoints 0")
    if not self.hooks:
      self.hooks.append(MallocCatcher("malloc@plt"))
      self.hooks.append(CallocCatcher("calloc@plt"))
      self.hooks.append(ReallocCatcher("realloc@plt"))
      self.hooks.append(FreeCatcher("free@plt"))
  def uninstall(self):
    while self.hooks:
      hook = self.hooks.pop()
      hook.delete()
    gdb.execute("set can-use-hw-watchpoints 1")
  def status(self):
    if self.hooks:
      return "on"
    else:
      return "off"

class Guards():
  def __init__(self):
    self.guards = []
    self.enabled = True
  def append(self, guard):
    self.guards.append(guard) 
  def clean(self):
    while self.guards:
      guard = self.guards.pop()
      guard.delete()
  def enable(self):
    self.enabled = True
  def disable(self):
    self.clean()
    self.enabled = False

guards = Guards()
hooks = Hooks()

"""
Command
"""
class Heapnosis( gdb.Command ):
  def __init__(self):
    super().__init__("heapnosis", gdb.COMMAND_DATA)

  def usage(self):
    print("heapnosis help -- display this")
    print("heapnosis enable -- install malloc/free catchers in order to track heap allocations")
    print("heapnosis disable -- remove malloc/free catchers")
    print("heapnosis status -- return heapnosis malloc/free catchers status")
    print("heapnosis chunks -- list/display/clean tracked allocations")
    print("heapnosis guards -- manage autoinstalled watchpoints that track memory corruptions")

  def chunks(self, args):
    global allocated_chunks
    if len(args) <= 1 or "help" == args[1]: 
      print("heapnosis chunks list -- list tracked allocations")
      print("heapnosis chunks <address> -- display a specific allocation history")
      print("heapnosis chunks clean -- remove tracks and guards")
    elif "list" == args[1]:
      for address in allocated_chunks:
        status = "freed" if allocated_chunks[address]["freed"] else "allocated"
        size = allocated_chunks[address]["size"]
        print("0x%x (0x%x bytes): %s"%(address, size, status))
    elif "clean" == args[1]:
      allocated_chunks = {}
      guards.clean()
    else:
      try:
        chunk = int(args[1], 16) 
      except:
        return
      print("\nChunk at 0x%x (size: 0x%x)"%(chunk, allocated_chunks[chunk]["size"]))
      display_allocation(chunk)  

  def guards(self, args):
    global guards
    if len(args) <= 1 or "help" == args[1]:
      print("heapnosis guards clean -- remove installed watchpoints")
      print("heapnosis guards disable -- do not install watchpoints")
      print("heapnosis guards enable -- <default> install watchpoints on guards")
    if "clean" == args[1]:
      guards.clean()
    elif "disable" == args[1]:
      guards.disable()
    elif "enable" == args[1]:
      guards.enable()
      
  def invoke(self, arg, from_tty):
    global hooks
    args = arg.split(" ")
    if args[0] == "" or "help" == args[0]:
      self.usage()
    elif "enable" == args[0]:
      hooks.install()
    elif "disable" == args[0]:
      hooks.uninstall()
    elif "status" == args[0]:
      print("heapnosis catchers are "+hooks.status())
    elif "chunks" == args[0]:
      self.chunks(args)
    elif "guards" == args[0]:
      self.guards(args)

Heapnosis()
